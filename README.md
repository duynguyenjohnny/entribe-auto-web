# README #

This README would normally document whatever steps are necessary to run test scripts and test scenarios for Client Entribe Project

### What is this repository for? ###

* This repository is automation framework to execute test scripts or test scenarios 
* Version 1.0.0

### How do I get set up? ###

* Summary of set up: Clone this source code, build maven and jdk to run
* How to run tests: right click on test scripts you want it run and run, or use testNG XML file to run test scenarios (bundle of test scritps)
* Deployment Schedule: Page Patern and POM, Extent Report, Integrated Zypher, Integrated CI/CD (Jenkins)

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact